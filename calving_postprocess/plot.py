import netCDF4 as nc
import numpy as np
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt

# Define a custom colormap with 26 discrete colors
initfilename='OUTPUT_FILES/INIT_exp1.nc'
filenamedefault="../EXP1/OUTPUT_FILES/2km_exp1_Ao9_C001_rhoi917_rhow1028_10400-10409.nc"
def plotUnstructured(filename=filenamedefault, variable="bedrock",variable_units="m"):
    colors = plt.cm.get_cmap('jet', 26)
    custom_cmap = ListedColormap(colors(np.linspace(0, 1, 256)))
    nc_file = nc.Dataset(filename,'r')
    my_var_data = nc_file.variables[variable][:]
    x = nc_file.variables['x'][:]
    y = nc_file.variables['y'][:]
    nc_file.close()
    fig = plt.figure()
    ax=fig.add_subplot(111)
    ax.set_aspect('equal')
    my_var_fig=plt.scatter(x/1000.,y/1000.,c=my_var_data,cmap=custom_cmap)
    plt.xlabel('x (km)')
    plt.ylabel('y (km)')
    plt.xlim([-800,800])
    plt.ylim([-800,800])
    plt.title(variable)
    #cbar=fig.colorbar()
    cbar=fig.colorbar(my_var_fig)
    cbar.set_label(variable_units, rotation=0)
    plt.show()


